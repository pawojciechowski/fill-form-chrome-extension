function runScript() {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.executeScript(tabs[0].id, { file: 'fillForm.js' });
  });
}

document.addEventListener('DOMContentLoaded', function() {
  var placeholdItButton = document.getElementById('placehold-it');
  placeholdItButton.addEventListener('click', function() {
    runScript();
  }, false);
}, false);
