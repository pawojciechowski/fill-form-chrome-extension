function runScript() {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.executeScript(tabs[0].id, { file: 'fillForm.js' });
  });
}

chrome.commands.onCommand.addListener(function(command) {
  if (command === 'fillForm') {
    runScript();
  }
});
