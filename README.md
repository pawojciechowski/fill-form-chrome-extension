# Fill form Chrome Extension

Simple auto-fill extension for development on Ctrl + Shift + F shourtcut.

1. Clone repository. 
2. Go to chrome://extensions.
3. Enable developer mode.
4. Click Load unpacked and choose fill-form-chrome-extension directory.
