(function() {
  const textInputs = document.querySelectorAll('input[type="text"]:not([name="email"])');
  textInputs.forEach((inputElem) => {
    const inputPlaceholder = inputElem.getAttribute('placeholder');
    if (inputElem.getAttribute('name') !== 'zipcode') {
      inputElem.value = inputPlaceholder;
    } else {
      inputElem.value = 12345;
    }

    inputElem.dispatchEvent(new Event('input'));
    inputElem.dispatchEvent(new Event('change'));
    inputElem.dispatchEvent(new Event('blur'));
  });

  const numberInputs = document.querySelectorAll('input[type="number"]:not([name="mobile_phone"])');
  numberInputs.forEach((inputElem) => {
    inputElem.value = 1234567;
    inputElem.dispatchEvent(new Event('input'));
    inputElem.dispatchEvent(new Event('change'));
    inputElem.dispatchEvent(new Event('blur'));
  });

  const mobilePhoneInputs = document.querySelectorAll('input[name="mobile_phone"]');
  mobilePhoneInputs.forEach((inputElem) => {
    inputElem.value = 123456789;
    inputElem.dispatchEvent(new Event('input'));
    inputElem.dispatchEvent(new Event('change'));
    inputElem.dispatchEvent(new Event('blur'));
  });

  const emailInputs = document.querySelectorAll('input[type="email"],input[name="email"]');
  emailInputs.forEach((inputElem) => {
    inputElem.value = 'admin@example.com';
    inputElem.dispatchEvent(new Event('input'));
    inputElem.dispatchEvent(new Event('change'));
    inputElem.dispatchEvent(new Event('blur'));
    inputElem.dispatchEvent(new Event('click'));
  });
})();
